<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class ShortenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required|url'
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $url = $this->url;

        if (!$url) {
            return;
        }

        if (Str::startsWith($url, 'https://')) {
            return;
        }

        if (Str::startsWith($url, 'http://')) {
            $url = str_replace('http://', 'https://', $url);
        } elseif (Str::startsWith($url, 'www')) {
            $url = 'https://' . $url;
        }

        $this->merge([
            'url' => $url,
        ]);
    }
}
