<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class URLResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'short_url' => route('home', ['shortUrl' => $this->short]),
            'url' => $this->long,
        ];
    }
}
