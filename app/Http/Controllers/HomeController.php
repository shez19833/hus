<?php

namespace App\Http\Controllers;

use App\Repositories\URLRepository;

class HomeController extends Controller
{
    public function __invoke(URLRepository $repository, String $shortUrl = null)
    {
        if ($shortUrl) {
            $url = $repository->getLongUrl($shortUrl);
            return redirect($url->long, 301);
        }

        return view('welcome');
    }
}
