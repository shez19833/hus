<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShortenRequest;
use App\Http\Resources\URLResource;
use App\Repositories\URLRepository;

class ShortenController extends Controller
{
    public function __invoke(URLRepository $repository, ShortenRequest $request)
    {
        $url = $repository->createShortUrl($request->url);

        return new URLResource($url);
    }
}
