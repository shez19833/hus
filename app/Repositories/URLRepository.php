<?php

namespace App\Repositories;

use App\Models\URL;

class URLRepository {

    public function getLongUrl($shortUrl)
    {
        return URL::whereShort($shortUrl)->firstOrFail();
    }

    public function createShortUrl($longUrl)
    {
        return URL::firstOrCreate([
            'long' => $longUrl
        ], [
            'short' => uniqid()
        ]);
    }
}
