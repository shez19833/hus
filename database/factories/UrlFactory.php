<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\URL;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(URL::class, function (Faker $faker) {
    return [
        'long' => $faker->url,
        'short' => config('app.url') . '/' . uniqid(),
    ];
});
