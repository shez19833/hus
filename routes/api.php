<?php

use App\Http\Controllers\ShortenController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/', ShortenController::class)->name('shorten')->middleware('throttle:10,1'); // 10 req in 1 minute
