<?php

namespace Tests\Feature;

use App\Models\URL;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class RedirectToUrlTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_am_forwarded_to_long_url_if_short_url_provided()
    {
        $url = factory(URL::class)->create([
            'short' => 'abc123',
            'long' => 'https://www.payasugym.com'
        ]);

        $this->get(route('home', ['shortUrl' => $url->short]))
            ->assertRedirect($url->long);
    }

    public function test_i_am_forwarded_to_404_if_short_url_is_invalid()
    {
        $this->get(route('home', ['shortUrl' => '123444']))
            ->assertStatus(404);
    }
}
