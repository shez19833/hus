<?php

namespace Tests\Feature;

use App\Models\URL;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class ShortenUrlTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_get_short_url_when_i_pass_url()
    {
        $longUrl = 'https://www.payasugym.com';

        $this->post(route('shorten'), ['url' => $longUrl])
            ->assertJsonStructure(['data' => [
                "url",
                "short_url"
            ]])
            ->assertJsonFragment(['url' => $longUrl]);
    }

    public function test_i_get_same_short_url_when_i_pass_repeated_url()
    {
        $url = factory(URL::class)->create([
            'short' => 'abc123',
            'long' => 'https://www.payasugym.com'
        ]);

        $this->post(route('shorten'), ['url' => $url->long])
            ->assertJson(['data' => [
               'short_url' => config('app.url') . '/' . $url->short,
               'url' => $url->long,
            ]]);
    }

    public function UrlVariationDataProvider()
    {
        return [
            ['http://www.payasugym.com'],
            ['https://www.payasugym.com'],
            ['www.payasugym.com'],
        ];
    }

    /**
     * @dataProvider UrlVariationDataProvider
     */
    public function test_i_get_same_short_url_when_i_pass_repeated_url_with_different_variations($longUrl)
    {
        $url = factory(URL::class)->create([
            'short' => 'abc123',
            'long' => 'https://www.payasugym.com'
        ]);

        $this->post(route('shorten'), ['url' => $longUrl])
            ->assertJson(['data' => [
               'short_url' => config('app.url') . '/' . $url->short,
               'url' => $url->long,
            ]]);
    }

    public function test_i_get_errors_if_no_url_passed()
    {
        $this->post(route('shorten'), ['url' => null])
           ->assertSessionHasErrors();
    }

    public function test_i_get_errors_if_invalid_url_passed()
    {
        $this->post(route('shorten'), ['url' => Str::random()])
            ->assertSessionHasErrors();
    }
}
