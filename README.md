<p align="center">Url Shortening Service</p>

## About 

This is tech test where the aim is to create a url shortening service. 

## Functionality

1. To return a shortened URL
2. To Redirect to full URL when a shortened URL is used

## SETUP

1. run `composer install`
2. run `npm install`
3. run `npm run dev`
4. copy .env.example to .env
5. run `touch database/database.sqlite`
6. run `php artisan migrate`
7. run `php artisan serve --port=8082`

Your app is now at: http://127.0.0.1:8082/

# TESTS 

run `vendor/bin/phpunit` 
